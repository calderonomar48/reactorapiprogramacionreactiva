package com.omar.reactor.app.reactor;


import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;

import reactor.core.publisher.Flux;

public class _08MonoIntervalDelayZipWith {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) throws InterruptedException {
		
		// EJEMPLO CON INTERVAL
//		System.out.println("iniciando main");
//		ejemploRangeZipWith();
//		hilo2();
//		System.out.println("main java");
		
		
		// EJEMPLO CON DELAYELEMENTS
		ejemploDelayElements();
		
		
	}
	
	public static void ejemploDelayElements() throws InterruptedException {
		
		Flux<Integer> numerosFlux = Flux.range(1, 1000)
				.delayElements(Duration.ofSeconds(1))
				.doOnNext(x -> System.out.println("deleyElements: " + x));
		numerosFlux.blockLast();
		
//		Thread.sleep(4080);
	}

	public static void ejemploRangeZipWith() {
		
		// creamos 2 streams, uno el intervalo de tiempo, el otro con 12 elementos
		
		Flux<Integer> numeros = Flux.range(1, 1000000);
		Flux<Long> retraso = Flux.interval(Duration.ofSeconds(0));
		
//		System.out.println("retraso " + retraso);
		
		// el fin de la programacion reactiva es escalar en diferentes hilos, 
		// nuestra clase main corre por un hilo, y por el delay que le hemos
		// dado el flux de numeros corre en otro hilo por 12 segundos
		// Los procesos y los flujos corren sin bloqueo, de manera asincrona.
		
//		numeros.zipWith(retraso, (ra, re) -> ra )
//			.doOnNext(x -> log.info(x.toString()))
//			.subscribe( x -> System.out.println("olaa") );
		
		// entonces vamos a forzar para que el stream sea con bloqueo para visualizar los numeros

		numeros.zipWith(retraso, (ra, re) -> ra )
			.doOnNext(x -> log.info(x.toString()))
			// el blockLast lo que hace es subscribir al flujo pero bloqueando el flujo
//			.subscribe();
			.blockLast();
		
		
		System.out.println("finaliza java");
		
	}
	
	public static void hilo2() {
		
		Flux<Integer> numeros = Flux.range(1, 100000);
		Flux<Long> retraso = Flux.interval(Duration.ofSeconds(0));
		
		numeros.zipWith(retraso, (ra, re) -> ra )
			.doOnNext(x -> log.info("hilo2: " + x.toString()))
			.blockLast();
		
		System.out.println("finaliza java hilo 2");
		
	}
	
}