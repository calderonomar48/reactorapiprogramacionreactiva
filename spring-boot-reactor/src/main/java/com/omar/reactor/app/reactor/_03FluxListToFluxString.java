package com.omar.reactor.app.reactor;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Usuario;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class _03FluxListToFluxString {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		
		// a partir de un collection, se va a usar el map para transformarlo a un String
		
		List<Usuario> lista = new ArrayList();
		lista.add(new Usuario("omar", "calderon"));
		lista.add(new Usuario("luis", "chang"));
		lista.add(new Usuario("luis", "aquino"));
		lista.add(new Usuario("ginna", "calderon"));
		
		// recibe una lista de usuarios y lo convierte a un String
		Flux.fromIterable(lista)
				// transformar la lista a un String
			.map( usuario ->
			{
				String nom = usuario.getNombre().toUpperCase().toString();
				String ape = usuario.getApellido().toUpperCase().toString();
				return nom.concat(" ").concat(ape);
			})
			// el flatMap se usa para filtrar en base a un nombre
			.flatMap(nombre -> { 
				if (nombre.contains("luis".toUpperCase())) {
					return Mono.just(nombre);	
				} else {
					return Mono.empty();
				}
			})
			// se hace la subscripcion, ejecuta el disposable()
			.subscribe(e -> log.info(e));
			
		
	}
	
}
