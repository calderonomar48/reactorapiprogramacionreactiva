package com.omar.reactor.app.util;

import com.omar.reactor.app.bean.Comentarios;
import com.omar.reactor.app.bean.Usuario;

import reactor.core.publisher.Mono;

public class MonoComentario {

	
	public static Mono<Usuario> obtieneUsuario(){
		return Mono.fromCallable(() -> new Usuario("omar", "calderon"));
	}
	
	public static Mono<Comentarios> obtieneComentarios(){
		return Mono.fromCallable( () -> {
			Comentarios comentarios = new Comentarios();
			comentarios.addComentario("hola pepe q tal");
			comentarios.addComentario("ya me voy");
			comentarios.addComentario("estamos en febrero");
			return comentarios;
		});	}
	
}
