package com.omar.reactor.app.reactor;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Usuario;

import reactor.core.publisher.Flux;

public class _04FluxObservableToFluxMono {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		// DESCRIPCION: en base a una lista, se recorrer la lista y se imprime, todo esto como un flujo de datos.
		
		List<Usuario> lista = new ArrayList();
		lista.add(new Usuario("omar", "calderon"));
		lista.add(new Usuario("luis", "chang"));
		lista.add(new Usuario("luis", "aquino"));
		lista.add(new Usuario("ginna", "calderon"));
		
		// recibe una lista de usuarios y lo convierte a un String
		Flux.fromIterable(lista)
				// el collectList espera a que se termine de hacer la carga de toda la data para recien enviarla.
				// la envia como un objeto unico, y dentro de ese objeto contiene la lista de objetos Usuarios
				.collectList()
				// ahora desde el subscribe si se puede imprimir uno por uno
				// un cliente puede recorrer el collectList
				.subscribe(listaUsuario -> {
					// el resultado es similar al iterable sin el collectList. 
					// la diferencia que es que se esta recorriendo un Flux<List<Usuario>> desde el observador
					log.info(listaUsuario.toString());
					// en cambio aca se esta recorriendo a traves de un foreach el
					listaUsuario.forEach( item -> log.info( item.toString()));
				});
				
		
	}
	
}
