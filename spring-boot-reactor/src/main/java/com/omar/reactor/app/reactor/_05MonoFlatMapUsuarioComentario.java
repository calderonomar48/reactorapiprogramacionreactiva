package com.omar.reactor.app.reactor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Comentarios;
import com.omar.reactor.app.bean.Usuario;
import com.omar.reactor.app.bean.UsuarioComentarios;
import com.omar.reactor.app.util.MonoComentario;

import reactor.core.publisher.Mono;

public class _05MonoFlatMapUsuarioComentario {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		
		// se va a crear un observable de Usuario, y otro observable de Comentarios de Usuario
		// con el flatMap se va a aplanar los 2 streams y se va a transformar en un solo stream UsuarioComentario()
		
//		Mono<Usuario> usuarioMono = Mono.fromCallable(() -> crearUsuario());
		
		// ya tenemos un flujo de datos, al usuario a traves Mono
		Mono<Usuario> usuarioMono = MonoComentario.obtieneUsuario();
		
//		usuarioMono.subscribe(System.out::print);
		
		// creamos otro flujo de datos para comentarios
		// serian los comentarios del usuario
		Mono<Comentarios> comentariosUsuarioMono = MonoComentario.obtieneComentarios();
		
		// el flatmap va a juntar los 2 stream, el de usuario y el de comentarios
		// flatMap va a aplanar al usuarioMono con sus comentarios y retornar un unico stream
//		usuarioMono.flatMap(u -> comentariosUsuarioMono.map(c -> new UsuarioComentarios(u, c)) );
		
//		usuarioMono.flatMap(usuario -> comentariosUsuarioMono
//				       .map(comentarios -> new UsuarioComentarios(usuario, comentarios) 
//	    		   ));
		
		Mono<UsuarioComentarios> monoUsuarioComment =
		usuarioMono.flatMap(u -> comentariosUsuarioMono.map(c -> new UsuarioComentarios(u, c)));
		
		monoUsuarioComment.subscribe( uc -> log.info(uc.toString()) );
		
	}
	
	public static Usuario crearUsuario() {
		return new Usuario("omar", "calderon");
	}
	
}
