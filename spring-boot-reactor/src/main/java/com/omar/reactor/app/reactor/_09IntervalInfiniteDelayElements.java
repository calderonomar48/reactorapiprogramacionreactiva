package com.omar.reactor.app.reactor;


import java.time.Duration;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;

import reactor.core.publisher.Flux;
import sun.util.logging.resources.logging;

public class _09IntervalInfiniteDelayElements {

	private static final Logger log = LoggerFactory.getLogger(_09IntervalInfiniteDelayElements.class);

	public static void main(String[] args) throws Exception {

		
		// se crea un observable y se aplica un delay de un segundo
		// CountDownLatch se usa para bloquear el hilo, solo hasta que termine su ejecucion
		intervalInfinito();
		
	}
	

	public static void intervalInfinito() throws InterruptedException {
		
		// se va a inicializar en 1, y se va  decrementar
		CountDownLatch latch = new CountDownLatch(1);
		
		Flux.interval(Duration.ofSeconds(1))
		.doOnTerminate(latch::countDown)
		.flatMap(x -> {
			if (x >= 5) {
				return Flux.error(new InterruptedException("Solo hasta 5"));
			}
			return Flux.just(x);
		})
		// intenta ejecutar el flujo nuevamente, una vez q falla
		.retry(1)
		.map(x -> "Hola " + x)
		.subscribe(s -> log.info(s),
				e -> log.error(e.getMessage()));
		
		// va a estar esperando hasta que el contador llegue a 0
		latch.await();
		
	}
	
}