package com.omar.reactor.app.reactor;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Usuario;

import reactor.core.publisher.Flux;

public class _02FluxCollections {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		
//		Flux<String> nombres = Flux.just("omar calderon", "luis evangelista", "ginna evangelista");

		List<String> lista = new ArrayList();
		lista.add("omar calderon");
		lista.add("luis evangelista");
		lista.add("ginna evangelista");
		
		
		// recibe un
		Flux<String> usuariosFlux = Flux.fromIterable(lista);
		
		usuariosFlux.map( nombre ->
		{
			String nom = nombre.split(" ")[0].toUpperCase().toString();
			String ape = nombre.split(" ")[1].toUpperCase().toString();
			return new Usuario(nom, ape);
		})
		.filter(usuario -> usuario.getApellido().toUpperCase().equalsIgnoreCase("evangelista") )
//		.doOnNext(x ->{
//			System.out.println(x.getNombre() + " " + x.getApellido());
//		})
		.map(usuario -> {
			String nombre = usuario.getNombre().toLowerCase();
			usuario.setNombre(nombre);
			return usuario;
		});
		
		// ************SUBSCRIPCION DEL OBSERVABLE
		usuariosFlux.subscribe(
				// success
			e -> { 
//				log.info(e.getNombre() + " " + e.getApellido());
				log.info(e);
				 },
			// error
			error -> log.error(error.getMessage(),
			// onComplete
			new Runnable() {
				@Override
				public void run() {
					log.info("FIN DEL OBSERVADOR, ES DECIR DEL SUBSCRIBER");
				}
			})
		);
		
	}
	
}
