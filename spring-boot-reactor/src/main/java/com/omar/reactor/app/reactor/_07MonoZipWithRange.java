package com.omar.reactor.app.reactor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;

import io.reactivex.Observable;
import reactor.core.publisher.Flux;

public class _07MonoZipWithRange {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		ejemploRangeZipWith();
		
	}

	public static void ejemploRangeZipWith() {
		
		Flux<Integer> rangoIni = Flux.just(1,2,3,4,5,6);
		Flux<Integer> rangoFin = Flux.range(1, 6);
		
		rangoIni
		// con el map estamos transformando
		.map(num -> num)
		// fusionando streams con zipWith
		.zipWith(rangoFin, (uno, dos) ->
			String.format("Primer Flux> %d, Segundo Flux: %d", uno, dos *2))
		// ejecutando el dispose
		.subscribe(x -> log.info(x));
		
	}
	
	public static void ejemploRangeFlatMap() {
		
		// tenemos 2 streams con un rango de n numeros y se va a juntar los 2 streams 
		// en uno solo y se va a imprimir un string
		
		Flux<Integer> rangoIni = Flux.just(1,2,3,4,5,8);
		Flux<Integer> rangoFin = Flux.range(1, 6);
		
		rangoIni
		// con el map estamos transformando
		.map(num -> { return num * 2; })
		// fusionando streams con flatMap
		.flatMap(ini -> rangoFin.map(fin ->  
			String.format("FLATMAP: Primer Flux> %d, Segundo Flux: %d", ini, fin)))
		// ejecutando el dispose
		.subscribe(x -> log.info(x));
		
		// CONCLUSION
		// el flatmap actua distinto, Por cada vuelta que ejecute "ini" se va a ejecutar la cantidad de items de "fin"
		
	}

	public static void ejemploBasicoRange() {
		
		Flux<Integer> num = Flux.range(1, 10);
		num.map(x -> x *2)
		.subscribe(x -> log.info("RESULTADO: " + x.toString()));
	
	}
	
	
	public static void ejemploBasicoRangeRxJava() {
		
		Observable<Integer> numerosObs = Observable.range(1, 4);
		
		numerosObs.subscribe(x -> log.info("RESULTADO: " + x.toString()));
	
	}
}






