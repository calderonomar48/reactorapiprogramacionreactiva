package com.omar.reactor.app.reactor;


import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import reactor.core.publisher.Flux;

public class _11Contrapresion {

	private static final Logger log = LoggerFactory.getLogger(_11Contrapresion.class);

	public static void main(String[] args) throws Exception {

		
		createFlux();
		
	}
	

	public static void createFlux() throws InterruptedException {
		
	
		Flux.create(emitter ->{
			
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				
				private Integer contador = 0;
				@Override
				public void run() {
					emitter.next(++contador);
					if (contador == 10) {
						timer.cancel();
						emitter.complete();
					}
					
					if (contador == 5) {
						timer.cancel();
						emitter.error(new InterruptedException("Error, se ha detenido el flux en 5"));
					}
					
				}
			}, 1000, 1000);
			
		})
		.doOnComplete( () -> log.info("hemos termiado"))
		.subscribe(next -> log.info(next.toString()),
				error -> log.error(error.getMessage()));
		
		
		
	}
	
}