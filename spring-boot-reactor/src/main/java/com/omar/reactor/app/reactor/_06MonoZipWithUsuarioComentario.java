package com.omar.reactor.app.reactor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Comentarios;
import com.omar.reactor.app.bean.Usuario;
import com.omar.reactor.app.bean.UsuarioComentarios;
import com.omar.reactor.app.util.MonoComentario;

import reactor.core.publisher.Mono;

public class _06MonoZipWithUsuarioComentario {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		
		zipWithForma1();
		
	}

	public static void zipWithForma1() {
		
		// se tiene 2 streams. Se va a fusionar con withZip y se va a crear UsuarioComentarios
		
		Mono<Usuario> usuarioMono = MonoComentario.obtieneUsuario();

		Mono<Comentarios> comentariosUsuarioMono = MonoComentario.obtieneComentarios();

		Mono<UsuarioComentarios> monoUC = usuarioMono.zipWith(comentariosUsuarioMono, (usuario, comentariosUsuario) -> 
			new UsuarioComentarios(usuario, comentariosUsuario)
		);
		
		monoUC.subscribe( uc -> log.info(uc.toString()));
		
	}

	public static void zipWithForma2() {
		
		Mono<Usuario> usuarioMono = MonoComentario.obtieneUsuario();

		Mono<Comentarios> comentariosUsuarioMono = MonoComentario.obtieneComentarios();

		usuarioMono.zipWith(comentariosUsuarioMono)
		           .map(tuple -> {
		        	  Usuario u = tuple.getT1(); 
		        	  Comentarios c  = tuple.getT2();
		        	  return new UsuarioComentarios(u, c);
		           });
		
//		Mono<UsuarioComentarios> monoUC = usuarioMono.zipWith(comentariosUsuarioMono, (usuario, comentariosUsuario) -> 
//			new UsuarioComentarios(usuario, comentariosUsuario)
//		);
//		
//		monoUC.subscribe( uc -> log.info(uc.toString()));
		
	}
	
}
