package com.omar.reactor.app.reactor;


import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.omar.reactor.app.SpringBootReactorApplication;
import com.omar.reactor.app.bean.Usuario;

import io.reactivex.Observable;
import reactor.core.publisher.Flux;

public class _01ReactorFluxBasico {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {

		//***************CREACION DEL OBSERVABLE
		
		//Estamos creando un publisher, es decir un observable, este observable emite items
		// los items que va a emititr sonde tipo string
		// se esta creando un flujo de dato inicial
		Flux<String> nombres = Flux.just("omar calderon", "luis evangelista", "ginna evangelista");
		
		// la inmutabilidad
		Flux<Usuario> usuariosFlux = nombres
//				.doOnNext( elemento -> {System.out.println(elemento);});
				// on next va a recorrer cada item de String 
				// el map hace la tranformacion del flujo de datos (stream)
				.map( nombre -> 
				{
					String nom = nombre.split(" ")[0].toUpperCase().toString();
					String ape = nombre.split(" ")[1].toUpperCase().toString();
					return new Usuario(nom, ape);
				})
				// el filter evalúa una expresion y solo en caso sea verdadera emitirá su valor y los mostrará
				// el filter es una operador intermedio
				.filter(usuario -> usuario.getApellido().toUpperCase().equalsIgnoreCase("evangelista") )
				// x representa a cada elemento del flujo de datos
				// en el doonnext va a recorrer cada item que emite desde el flujo de datos
				// el item que va a emitir doOnNExt va a ser del tipo de objeto que se trnsformado en el MAP
				.doOnNext(x ->{
					System.out.println(x.getNombre() + " " + x.getApellido());
				})
				// nuevamente se hace una transformacion
				.map(usuario -> {
					String nombre = usuario.getNombre().toLowerCase();
					usuario.setNombre(nombre);
					return usuario;
				});
		
		
		// ************SUBSCRIPCION DEL OBSERVABLE
		
		// no va a imprimir nada, porque no hay un subscriptor
		// creamos un observador o un subscriber
		usuariosFlux.subscribe(
			
			//on Success
			e -> { 
				log.info(e.getNombre() + " " + e.getApellido());
//					log.info("IMPRIMIENDO DATOS EN EL LOG EN EL SUBSCRIBER");
//					log.info("NOMBRE: " + e.getNombre());
//					log.info("APELLIDO " + e.getApellido());
//					log.info("FIN IMPRESION...........");
				 },
			
			// on error
			error -> log.error(error.getMessage(),
			
			// el tercer parametro del FLUX es para el ejecutar alguna acción al final del proceso
		    // oncomplete 
			new Runnable() {
				@Override
				public void run() {
					log.info("FIN DEL OBSERVADOR, ES DECIR DEL SUBSCRIBER");
				}
			})
		);
		
		//OBSERVABLES IOREACTIVE
//		String nombreArray[] = ["jean","carlos","maria"];
		Observable<String> nombreObs = Observable.just("carlos","","juan","maria")
				.doOnNext(cadena -> {
					if ( !Strings.isNotBlank(cadena)) {
						throw new Exception("error de nuevo, cadena vacia");
					}
					System.out.println(cadena);
				});
		
		nombreObs.subscribe( x -> log.info(x)
				);
		
	}
	
}
